import React from 'react';
import ReactDOM from 'react-dom';
import { Stage, Sprite, Text, Container } from '@inlet/react-pixi';
import * as PIXI from 'pixi.js';
import KeyboardEventHandler from 'react-keyboard-event-handler';
import planeImage from './img/plane_1.png';
import planet1Image from './img/planet_1.png';
import planet2Image from './img/planet_2.png';
import planet3Image from './img/planet_3.png';
import logoImage from './img/Logo_Blk.png';
import soundfile from './sound/bulletSound.wav';


class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      width : 0,
      height : 0,
      planePosition : {
        x : 100,
        y : 100
      },
      mousePosition : {
        x : 0,
        y : 0
      },
      click : 0,
      locationClicked : 0,
      planetPosition : [0,0,0,0,0,0],
    };
    this.soundShoot = new Audio(soundfile);
  };

  componentDidMount() {
    this.updateWindowDimensions();
    window.addEventListener('resize', this.updateWindowDimensions);
  }
  
  componentWillUnmount() {
    window.removeEventListener('resize', this.updateWindowDimensions);
  }
  
  updateWindowDimensions = () => {
    var planetPosition = this.state.planetPosition;
    planetPosition = [window.innerWidth / 2, 100, window.innerWidth / 2, window.innerHeight / 2 - 50, window.innerWidth / 2, window.innerHeight - 200]
    this.setState({ 
      width : window.innerWidth, 
      height: window.innerHeight,
      planetPosition 
    });
  }

  planeMoveMouse = () => {
    var click = this.state.click + 1;
    if (click > 1 && (this.state.mousePosition.x === this.state.locationClicked || (this.state.mousePosition.x - this.state.locationClicked <= 5 && this.state.mousePosition.x - this.state.locationClicked > 0) || (this.state.locationClicked - this.state.mousePosition.x <= 5 && this.state.locationClicked - this.state.mousePosition.x > 0))) {
      // bullet = new Sprite.from('bulletImage')
      // if (app.renderer.screen.width < 768) {
      //   bullet.scale.x = 0.3;
      //   bullet.scale.y = 0.3;
      //   bullet.position.x = plane1.x + plane1.width - 30;
      //   bullet.position.y = plane1.y - 5;
      // } else {
      //   bullet.scale.x = 0.5;
      //   bullet.scale.y = 0.5;
      //   bullet.position.x = plane1.x + plane1.width - 40;
      //   bullet.position.y = plane1.y - 15;
      // }
      // app.stage.addChild(bullet);
      // bullets.push(bullet);
      this.soundShoot.play();
      alert("shoot")
    } else {
      this.setState({ planePosition : { x : this.state.mousePosition.x, y: this.state.mousePosition.y } });
    }
  
    this.setState({ locationClicked : this.state.mousePosition.x, click});
  
    if (click === 1) {
      setTimeout(function () {
        click = 0
      }, 1000);
    }

  }
  
  planeMoveKeyboard = (key, e) => {
    var planePosition = this.state.planePosition;
    if (key === "space"){
      this.soundShoot.play();
      alert("shoot");
    } else if(key === "up"){
      planePosition.y -= 5;
    } else if ( key === "down"){
      planePosition.y += 5;
    } else if ( key === "right"){
      planePosition.x += 5;
    } else if ( key === "left"){
      planePosition.x -= 5;
    }

    this.setState({ planePosition });
  }

  _onMouseMove(e) {
    this.setState({ mousePosition : { x : e.nativeEvent.offsetX, y: e.nativeEvent.offsetY } });
  }

	render() {
		return(
      <Stage 
        width={this.state.width} 
        height={this.state.height} 
        options={{ backgroundColor: 0x012b30 }}
        onMouseMove={this._onMouseMove.bind(this)}
      >
        <Container
        hitArea = { new PIXI.Rectangle(0, 0, this.state.width, this.state.height)}
        interactive={true}
        pointerdown={this.planeMoveMouse}
        >
          <KeyboardEventHandler
            handleKeys={['space', 'up', 'down', 'left', 'right']}
            onKeyEvent={(key, e) => this.planeMoveKeyboard(key, e)} 
          />

          <Sprite 
            image={planeImage} 
            x={this.state.planePosition.x} 
            y={this.state.planePosition.y} 
            anchor={[0.5, 0.5]}
          />
          <Sprite
            scale={0.4}
            image={planet1Image} 
            x={this.state.planetPosition[0]} 
            y={this.state.planetPosition[1]} 
            anchor={[0.5, 0.5]}
          />
          <Text 
            x={this.state.planetPosition[0] + 100} 
            y={this.state.planetPosition[1] - 18} 
            text="Answer1"
          />
          <Sprite 
            scale={0.4}
            image={planet2Image} 
            x={this.state.planetPosition[2]} 
            y={this.state.planetPosition[3]} 
            anchor={[0.5, 0.5]}
          />
          <Text 
            x={this.state.planetPosition[2] + 100} 
            y={this.state.planetPosition[3] - 18} 
            text="Answer2"
          />
          <Sprite 
            scale={0.4}
            image={planet3Image} 
            x={this.state.planetPosition[4]} 
            y={this.state.planetPosition[5]} 
            anchor={[0.5, 0.5]}
          />
          <Text 
            x={this.state.planetPosition[4] + 100} 
            y={this.state.planetPosition[5] - 18} 
            text="Answer1"
          />

          <Text 
            x={50} 
            y={this.state.height - 50} 
            text="Level : 1"
          />
          <Text 
            x={this.state.width - 420} 
            y={this.state.height - 50} 
            text="Powered by :"
          />
          <Sprite 
            image={logoImage} 
            x={this.state.width - 250} 
            y={this.state.height - 60} 
          />
        </Container>
      </Stage>
		);
	}
}

export default App;
